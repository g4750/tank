// Fill out your copyright notice in the Description page of Project Settings.


#include "ToonTanksPlayController.h"
#include "GameFramework/Pawn.h"

void AToonTanksPlayController::SetPlayerEnabledState(bool bPlayerEnbled){
    if(bPlayerEnbled){
        GetPawn()->EnableInput(this);
    }else{
        GetPawn()->DisableInput(this);
    }
    bShowMouseCursor = bPlayerEnbled;
}

